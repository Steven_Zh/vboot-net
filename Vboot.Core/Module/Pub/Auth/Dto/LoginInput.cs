﻿namespace Vboot.Core.Module.Pub
{
    public class LoginInput
    {
        /// <example>vben</example>
        public string username{ get; set; }

        /// <example>123456</example>
        public string password{ get; set; }
    }
}